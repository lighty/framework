<!-- ![alt tag](https://raw.githubusercontent.com/fiesta-framework/Art/master/Resources/Pikia%20Github.png) -->
# Lighty v3.2

---

<!-- [![Build Status](https://travis-ci.org/fiesta-framework/Fiesta.svg?branch=next)](https://travis-ci.org/fiesta-framework/Fiesta/branches) -->
[![Latest Stable Version](https://poser.pugx.org/lighty/lighty/v/stable)](https://packagist.org/packages/lighty/lighty) 
[![Downloads](https://img.shields.io/badge/downloads-637-0375b5.svg)](https://gitlab.com/lighty/framework)
[![Latest Unstable Version](https://poser.pugx.org/lighty/lighty/v/unstable)](https://packagist.org/packages/lighty/lighty) 
[![License](https://poser.pugx.org/lighty/lighty/license)](https://packagist.org/packages/lighty/lighty)
<!--[![Monthly Downloads](https://poser.pugx.org/lighty/lighty/d/monthly)](https://packagist.org/packages/lighty/lighty)-->
<!-- [![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/fiesta-framework/Fiesta/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/fiesta-framework/Fiesta/?branch=master) -->

-----

![alt tag](https://gitlab.com/lighty/framework/raw/dev/app/resources/images/window.png)

-----

**Lighty** is a PHP framework made for web developers to make the web programing more easyer, faster and wonderful. Created and developed since 10/10/2014 by [Youssef Had](https://www.facebook.com/yussef.had), provided nice features to help in you in your project such as Lumos, Panel, Atomium.

Lighty Framework is one of [Lighty Group](https://gitlab.com/groups/lighty) projects ([Framework](https://gitlab.com/lighty/framework) - [Kernel](https://gitlab.com/lighty/kernel) - [Panel](https://gitlab.com/lighty/panel) - Bridge - [Whoops](https://gitlab.com/lighty/whoops) - Logy - [Installers](https://gitlab.com/lighty/installers) - Atomium - Lumos - Console - [Docs](https://gitlab.com/lighty/docs)) some of them are public and free and others aren't.

### Installation

#### Install Composer

Lighty uses `Composer`, You can use Composer  to install Lighty and its dependencies.

First, download and install [Composer installer](https://getcomposer.org/)

#### Install last stable release of Lighty

You can install **free** release of Lighty via [Composer](https://getcomposer.org/) by running the command of Composer `create-project` in your terminal:

	composer create-project lighty/lighty projet_name --prefer-dist

### Lighty Requirements

Lighty has some system requirements:
* PHP >= 5.5
* Enabling The mod_rewrite Apache extension

###  Apache

Lighty comes with `.htaccess` file that uses URL rewriting, if you use Apache, Be sure you have enabled the extension `mod_rewrite`

### Documentation

We are working on Lighty documentation for every release version, you can take look or update the documentation of 3.2 release [here](https://gitlab.com/lighty/Docs/tree/3.2)


### Development

Lighty is open to the contributions of developers, the current released version of Lighty is `3.2` . However developers may instead opt to use the next beta version in [dev](https://gitlab.com/lighty/framework/tree/dev) branch aliased to `3.x-dev`.

###### Install next beta release of Lighty

You can install next release of Lighty in beta via [Composer](https://getcomposer.org/) by running this command in your terminal:

	composer create-project lighty/lighty projet_name dev-dev --prefer-dist


### Versions

To consult the change log of the framework please visit here [change log](https://gitlab.com/lighty/framework/blob/dev/changes.md)

To get older releases of lighty framework please [Lighty Releases](https://gitlab.com/lighty/framework/tags)

### They use Lighty

[![Ipixa](https://gitlab.com/lighty/Art/raw/master/Clients/ipixa.png)](http://www.ipixa.net) 
[![Touhfa](https://gitlab.com/lighty/Art/raw/master/Clients/touhfa.png)](http://www.touhfat.com) 
[![Stabel](https://gitlab.com/lighty/Art/raw/master/Clients/stabel.png)](http://www.stabel.com) 
[![BMe](https://gitlab.com/lighty/Art/raw/master/Clients/bme.png)](http://bureaumercier.com/) 

### Licence

The Lighty framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

[![Owner](https://img.shields.io/badge/created%20by-Youssef%20Had-blue.svg)](https://gitlab.com/u/youssefhad)
[![Owner](https://img.shields.io/badge/copyright-2014--2016-red.svg)](https://gitlab.com/lighty/framework)
[![Owner](https://img.shields.io/badge/launched-10%2F10%2F2014-ff2f6c.svg)](https://gitlab.com/lighty/framework)

