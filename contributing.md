### Development

Lighty is open to the contributions of developers, the current released version of Lighty is `3.1` . However developers may instead opt to use the next beta version in [dev](https://gitlab.com/lighty/framework/tree/dev) branch aliased to `3.x-dev`.