<?php 

return array(

	/*
	|----------------------------------------------------------
	| Allow Debug
	|----------------------------------------------------------
	|  Here to make the framework shows errors and
	|  exceptions, false to show friendly messages
	|  and true to debug
	*/

	'debug' => ! false,

	/*
	|----------------------------------------------------------
	| Error Debug Message
	|----------------------------------------------------------
	|  If loggin.debug was false the framework will
	|  show this message
	*/

	'msg' => "Ohlala! il semble que quelque chose s'ait mal passé",

	/*
	|----------------------------------------------------------
	| Error log
	|----------------------------------------------------------
	|  The path of log file where Pikia store errors
	|  by default the framework use this path 
	|  'app/storage/logs/lighty.log'
	*/

	'log' => 'app/storage/logs/lighty.log',

	/*
	|----------------------------------------------------------
	| Error simple page background color
	|----------------------------------------------------------
	|  The color background of simple page error
	*/

	'bg' => '#a4003a',


);
?>