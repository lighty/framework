<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Encryption Keys
	|--------------------------------------------------------------------------
	|  These keys are for the security of your app, the first should be string
	|  contains 32 chars and the second should be string contains at least 10
	|  chars, in first configuration the framework change automatically these
	|  keys
	*/

	'key1' => '8aae522b95242e3618f6ecfc3e98c1b5',
	'key2' => 'a058f845231de632b6c25a2accb99ae8',

	
	
	/*
	|--------------------------------------------------------------------------
	| Panel Ajax Prefix
	|--------------------------------------------------------------------------
	|  The prefix for ajax route of panel
	*/

	'prefix' => 'f2457d663fb0a2258e1172fd763bd6ad_',

);