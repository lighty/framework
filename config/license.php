<?php 


return array(

	/*
	|----------------------------------------------------------
	| License of the project
	|----------------------------------------------------------
	*/

	'authorized'=>"true",
	'pageblock'=>"false",

	/*
	|----------------------------------------------------------
	| Blocked Messages
	|----------------------------------------------------------
	*/

	'webResponse'=>"this website is <b>not authorised</b> in the internet, please if you think that's an error try again",

	'pageMsg'=>"this page is <b>not authorised</b> in the internet, please if you think that's an error try again",

);