<?php 


return array(

	/*
	|----------------------------------------------------------
	| Default lang
	|----------------------------------------------------------
	| default framework language 
	*/

	'default'=>'fr',

	/*
	|----------------------------------------------------------
	| Lang Cookie name
	|----------------------------------------------------------
	| Langue cookie to store framework default language
	*/

	'cookie'=>'lighty_lang',

);