<?php 

/*
|----------------------------------------
| Lighty (https://gitlab.com/lighty)
| Copyright 2016 Youssef Had, Inc.
| Licensed under MIT License
|----------------------------------------
*/


/*
|----------------------------------------------
| Framework calling
|----------------------------------------------
| Calling the Lighty framework
*/

require __DIR__.'/../vendor/autoload.php';

/*
|----------------------------------------------
| Run the Framework
|----------------------------------------------
| launch the Lighty framework
*/

Lighty\Kernel\Foundation\Application::run();